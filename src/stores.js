import { writable } from "svelte/store";

export const name = writable("");
export const email = writable("");
export const link = writable("");
export const message = writable("");
